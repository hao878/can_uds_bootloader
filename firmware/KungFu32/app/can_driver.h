/******************************************************************************
 *                  Shanghai ChipON Micro-Electronic Co.,Ltd                  *
 ******************************************************************************
 *  $File Name$       : system_init.h                                         *
 *  $Author$          : ChipON AE/FAE Group                                   *
 *  $Data$            : 2021-07-8                                             *
 *  $AutoSAR Version  : V1.0	                                              *
 *  $Description$     : This file contains the Header file configuration 	  *
 * 						for KF32A156 device                                  *
 ******************************************************************************
 *  Copyright (C) by Shanghai ChipON Micro-Electronic Co.,Ltd                 *
 *  All rights reserved.                                                      *
 *                                                                            *
 *  This software is copyright protected and proprietary to                    *
 *  Shanghai ChipON Micro-Electronic Co.,Ltd.                                 *
 ******************************************************************************
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *                     		REVISON HISTORY                               	  *
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *  Data       Version  Author        Description                             *
 *  ~~~~~~~~~~ ~~~~~~~~ ~~~~~~~~~~~~  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  *
 *  2021-07-08 00.01.00 FAE Group     Version 2.0 update                      *
 *                                                                            *
 *                                                                            *
 *****************************************************************************/
#ifndef _CAN_DIVER_H_
#define _CAN_DIVER_H_
/******************************************************************************
**                       	Include Files                                     *
******************************************************************************/
#include <stdint.h>
#include "kf32a156_can.h"

/******************************************************************************
*                        Configuration definition                             *
******************************************************************************/
#define BAUDRATE100K    (0U)
#define BAUDRATE250K    (1U)
#define BAUDRATE500K    (2U)

#define BAUDRATE    BAUDRATE500K

/******************************************************************************
*                        Type  definitions                                    *
******************************************************************************/
#define INTERRUPT_ENABLE   1
#define INTERRUPT_DISABLE  0

/**
 * @brief  CAN消息结构体定义
 */
typedef  struct {
  unsigned int ID;
  struct {
    unsigned char IDE:1;///<0-标准帧，1-扩展帧
    unsigned char RTR:1;///<0-数据帧，1-远程帧
    unsigned char DIR:1;///<0-接收数据帧，1-发送数据帧
    unsigned char DLC:4;
  }Flags;
  unsigned char  Data[8];
}CAN_MSG;

typedef struct
{
	CAN_MessageTypeDef CAN_Message[64];
	uint8_t Frame_length;
}Can_Pdu_TypeDef;

typedef struct
{
	uint32_t CAN_TRANSMIT_INTERRUPT;
	uint32_t CAN_RECEIVE_INTERRUPT;
	uint32_t CAN_OVERFLOW_INTERRUPT;
	uint32_t CAN_BUSERROR_INTERRUPT;
	uint32_t CAN_BUSOFF_INTERRUPT;
	uint32_t CAN_ERRORNEGATIVE_INTERRUPT;
}Can_Interrupt_Type;

/******************************************************************************
*                      Functional definition                                 *
******************************************************************************/
void CAN_Gpio_Init(void);
void CAN_Init(CAN_SFRmap* CANx, CAN_InitTypeDef* canInitStruct);
RetStatus CAN_Transmit_Message_Once(volatile CAN_MessageTypeDef *CAN_Message);
RetStatus CAN_Transmit_Message_Repeat(volatile CAN_MessageTypeDef *CAN_Message);
RetStatus CAN_Receive_Message(volatile Can_Pdu_TypeDef* Pdu);
void CAN_Int_Config(Can_Interrupt_Type *Can_Interrupt);
void CAN_Configuration(void);
uint8_t CAN_WriteData(uint8_t Channel,CAN_MSG *pMsg);
#endif

